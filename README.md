# 15EX05_data_correction

Correction of the data 15EX05 (presented in the data paper https://doi.org/10.1051/ocl/2020027)

# Organization

- [scripts/Analysis_Fluidigm_15EX05_Pl01_10.m](./scripts/Analysis_Fluidigm_15EX05_Pl01_10.m) : Script matlab for Amplification efficiency and Ct normalization. The entry data for this script is not provided.

- data/Analysis_Fluidigm_15EX05_results_export_20180430-112453.txt : Resulting dataset from the previous script

- [scripts/1_correct_plate_effect.R](./scripts/1_correct_plate_effect.R) : R script to correct the plate effect, on the previous dataset.

- [scripts/2_correct_field_effect.R](./scripts/2_correct_field_effect.R) : R script to correct the field effect, on the exit file from the previous script.

- [scripts/3_handling_missings.R](./scripts/3_handling_missings.R) : R script to correct the plate effect, on the exit file from the previous script.

