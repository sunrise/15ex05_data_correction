%% Charger les donnees
rootPath = 'P:\Priv\COMMUN\PROJETS\SUNRISE\WP4_et_6_Biologie_des_systemes\Biologie_Moleculaire\15EX05\Analyse puces\MATLAB-dCt';
dataPath = 'P:\Priv\COMMUN\PROJETS\SUNRISE\WP4_et_6_Biologie_des_systemes\Biologie_Moleculaire\15EX05\Analyse puces\Fichiers de sortie FLUIDIGM excel';
resPath = 'P:\Priv\COMMUN\PROJETS\SUNRISE\WP4_et_6_Biologie_des_systemes\Biologie_Moleculaire\15EX05\Analyse puces\MATLAB-dCt\results';
% dataPath = '/mnt/GRP_GGT/GRP_PV/Priv/COMMUN/PROJETS/SUNRISE/WP4_et_6_Biologie_des_systemes/Biologie_Moleculaire/15EX05/Analyse puces/Fichiers de sortie FLUIDIGM excel';
% resPath = ['/mnt/GRP_GGT/GRP_PV/Priv/COMMUN/PROJETS/SUNRISE/WP4_et_6_Biologie_des_systemes/Biologie_Moleculaire/15EX05/Analyse puces/MATLAB-dCt','/results'];
mkdir(resPath);
delimiter = ';';
startRow = 11;

%% Format for each line of text:
%   column1: text (%s)
%	column2: text (%s)
%   column3: text (%s)
%	column4: text (%s)
%   column5: text (%s)
%	column6: text (%s)
%   column7: text (%s)
%	column8: text (%s)
%   column9: text (%s)
%	column10: text (%s)
%   column11: text (%s)
%	column12: text (%s)
%   column13: text (%s)
%	column14: text (%s)
% For more information, see the TEXTSCAN documentation.
formatSpec = '%s%s%s%s%s%s%s%s%s%s%s%s%s%s%[^\n\r]';

%% List data files.
% lister fichier dans dossier
fileList = dir(fullfile(dataPath,'*.csv'));
% rawdata = table;

%% 1st file
fileID = fopen(fullfile(dataPath,fileList(1).name),'r');
textscan(fileID, '%[^\n\r]', startRow-1, 'WhiteSpace', '', 'ReturnOnError', false, 'EndOfLine', '\r\n');
dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'EmptyValue', NaN, 'ReturnOnError', false);
fclose(fileID);

% Create output variable
rawdata.Plate = repmat(string(fileList(1).name(1:3)),size(dataArray{:, 1}));
rawdata.ChamberID = dataArray{:, 1};
rawdata.SampleName = dataArray{:, 2};
rawdata.SampleType = dataArray{:, 3};
rawdata.SamplerConc = str2double(strrep(dataArray{:,4},',','.'));
rawdata.GeneName = dataArray{:, 5};
rawdata.GeneType = dataArray{:, 6};
rawdata.CtValue = str2double(strrep(dataArray{:, 7},',','.'));
rawdata.CtCalibratedrConc = str2double(strrep(dataArray{:, 8},',','.'));
rawdata.CtQuality = str2double(strrep(dataArray{:, 9},',','.'));
rawdata.CtCall = dataArray{:, 10};
rawdata.CtThreshold = str2double(strrep(dataArray{:, 11},',','.'));
rawdata.TmInRange = str2double(strrep(dataArray{:, 12},',','.'));
rawdata.TmOutRange = str2double(strrep(dataArray{:, 13},',','.'));
rawdata.TmPeakRatio = str2double(strrep(dataArray{:, 14},',','.'));
    
%% Other files    
for i = 2:numel(fileList)
    fileID = fopen(fullfile(dataPath,fileList(i).name),'r');
    textscan(fileID, '%[^\n\r]', startRow-1, 'WhiteSpace', '', 'ReturnOnError', false, 'EndOfLine', '\r\n');
    dataArray = textscan(fileID, formatSpec, 'Delimiter', delimiter, 'TextType', 'string', 'EmptyValue', NaN, 'ReturnOnError', false);

    % Close the text file.
    fclose(fileID);

    % Concatenate output variable
    rawdata.Plate = [rawdata.Plate; repmat(string(fileList(i).name(1:3)),size(dataArray{:, 1}))];
    rawdata.ChamberID = [rawdata.ChamberID; dataArray{:, 1}];
    rawdata.SampleName = [rawdata.SampleName; dataArray{:, 2}];
    rawdata.SampleType = [rawdata.SampleType; dataArray{:, 3}];
    rawdata.SamplerConc = [rawdata.SamplerConc; str2double(strrep(dataArray{:,4},',','.'))];
    rawdata.GeneName = [rawdata.GeneName; dataArray{:, 5}];
    rawdata.GeneType = [rawdata.GeneType; dataArray{:, 6}];
    rawdata.CtValue = [rawdata.CtValue; str2double(strrep(dataArray{:, 7},',','.'))];
    rawdata.CtCalibratedrConc = [rawdata.CtCalibratedrConc; str2double(strrep(dataArray{:, 8},',','.'))];
    rawdata.CtQuality = [rawdata.CtQuality; str2double(strrep(dataArray{:, 9},',','.'))];
    rawdata.CtCall = [rawdata.CtCall; dataArray{:, 10}];
    rawdata.CtThreshold = [rawdata.CtThreshold; str2double(strrep(dataArray{:, 11},',','.'))];
    rawdata.TmInRange = [rawdata.TmInRange; str2double(strrep(dataArray{:, 12},',','.'))];
    rawdata.TmOutRange = [rawdata.TmOutRange; str2double(strrep(dataArray{:, 13},',','.'))];
    rawdata.TmPeakRatio = [rawdata.TmPeakRatio; str2double(strrep(dataArray{:, 14},',','.'))];
end
%% Clear temporary variables
clearvars delimiter startRow formatSpec fileID dataArray ans;

%% Remplacer les echantillons echoues, notes 999 par Fluidigm par, NaN
rawdata.CtValue(rawdata.CtValue==999) = NaN;

%% Creer sous-variables
SName = rawdata.SampleName; % prendre la colonne SampleName
SType = rawdata.SampleType; % prendre la colonne SampleType
CG=cellstr(rawdata.GeneName); % GeneName
Ct=rawdata.CtValue; % CtValue
rConc = rawdata.SamplerConc;
Pl = rawdata.Plate;
N=size(rawdata,1); % calcul du nombre de puits

% Charger les options de bioinfo
import bioma.data.*

%% Renommer les echantillons presents sur toutes les plaques
% tic
SName = replace(SName, 'SF092_SF342 (500) T-','500');
SName = replace(SName, 'LG5450HO (18) T-','61');
SName = replace(SName, 'LG5450HO (61) T-','61');
SName = replace(SName, 'pool (gamme de dil,)','gamme');
SName = replace(SName, 'pool (dil 1/16)','gamme');
SName = replace(SName, 'pool (dil 1/8)','gamme');
SName = replace(SName, 'pool (dil 1/4)','gamme');
SName = replace(SName, 'pool (dil 1/2)','gamme');
SName = replace(SName, 'pool (dil 1)','gamme');
SName = replace(SName, 'pool (dil 1/2)','gamme');
SName = replace(SName, 'H2O (équipe)','H2O');

CG = replace(CG,'RNAse P','RNAseP');
CG = replace(CG,'HanXRQChr04g0115631','HanXRQChr04g0115631=refnew');
CG = replace(CG,'HanXRQChr03g0090171','HanXRQChr03g0090171=refnew');
CG = replace(CG,'HanXRQChr01g0021131','HanXRQChr01g0021131=refnew');
% CG = replace(CG,'HanXRQChr05g0131911=ref','HanXRQChr05g0131911=ref');
% CG = replace(CG,'HanXRQChr01g0029581=ref','HanXRQChr01g0029581=ref');

% toc
%% Grouper les donnees
[CG_G, CG_GN] = grp2idx(CG);
[SN_G, SN_GN] = grp2idx(SName);
[ST_G, ST_GN] = grp2idx(SType);
[Pl_G, Pl_GN] = grp2idx(Pl);

%% Note
% Controle intene de la plateforme
%   SampleName = ADNg
%   GeneName = RNAseP
% Echantillon eau = SampleName = H2O mais aussi SampleType = NTC
% Gamme etalon = SampleType = Standard
% Sample Type
%   NTC = eau
%   Unknown = echantillons
%   reference = controle interne plateforme ADNg
%   Standard = gamme etalon
% 
%% Calculer l'efficacite
% Calculer l'efficacite
eff = nan(size(CG_GN));
start = 1; % Charlotte changer ici pour redemarrer
for i = start:numel(CG_GN)
   
    if strcmp(CG_GN{i},'RNAseP')
        eff(i)=0;
% --- Copier depuis ici ---                
    elseif i == 28 % Modifier avec numero du gene
        bad = 17; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 44 % Modifier avec numero du gene
        bad = 6; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 47 % Modifier avec numero du gene
        bad = 16; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 57 % Modifier avec numero du gene
        bad = 11; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));  
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 62 % Modifier avec numero du gene
        bad = 18; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 71% Modifier avec numero du gene
        bad = 16; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)  
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
     elseif i == 73 % Modifier avec numero du gene
        bad = [16,17,18,19,20]; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 104 % Modifier avec numero du gene
        bad = 16; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 106 % Modifier avec numero du gene
        bad = 18; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 108 % Modifier avec numero du gene
        bad = 21; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 116 % Modifier avec numero du gene
        bad = 1; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 119 % Modifier avec numero du gene
        bad = [2,17]; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
      elseif i == 131 % Modifier avec numero du gene
        bad = 21; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 132 % Modifier avec numero du gene
        bad = 19; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 135 % Modifier avec numero du gene
        bad = 2; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 149 % Modifier avec numero du gene
        bad = 2; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 162 % Modifier avec numero du gene
        bad = 21; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 180 % Modifier avec numero du gene
        bad = 12; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 184 % Modifier avec numero du gene
        bad = 22; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 38 % Modifier avec numero du gene
        bad = [7,12,18,19,20]; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 62 % Modifier avec numero du gene
        bad = [18,21,22,24,25]; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 97 % Modifier avec numero du gene
        bad = [13,18,24]; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 105 % Modifier avec numero du gene
        bad = [19,22]; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 167 % Modifier avec numero du gene
        bad = [1,6,8,11,23,25]; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
    elseif i == 175 % Modifier avec numero du gene
        bad = 22; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];

    elseif i == 186 % Modifier avec numero du gene
        bad = 12; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
    elseif i == 188 % Modifier avec numero du gene
        bad = [11,13,16]; % Modifier avec numero des points si 1 = [17], si +sieurs [17,23]
        ctmix = Ct(CG_G==i & strcmp('gamme',SName));
        % Recuperer les dilutions de la courbe
        dil = rConc(CG_G==i & strcmp('gamme',SName));
        
        % Enlever mauvaises donnees
        ctmix_ori = ctmix; dil_ori = dil;
        ctmix(bad) = [];
        dil(bad) = [];
        
        % Calculer la pente b(2) intersection b(1) 
        b = robustfit(log10(1./dil'),ctmix);
        % Calculer l'efficacite
        eff(i) = 10^(-1/b(2));

        % Dessiner la figure
        figure
        k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
        k_old = scatter(log10(1./dil_ori(bad)'),ctmix_ori(bad),'rx');
        x = log10(1./dil');
        plot(x,b(1)+b(2)*x,'g','LineWidth',2)
        title ({['Gene ',num2str(i)];CG_GN{i};['Corrected efficacity = ',num2str(eff(i),'%0.03f')]})
        ax = gca;
        ax.YLabel.String = 'C_t';
        ax.XLabel.String = 'Dilution';
        ax.XTick = sort(unique(log10(1./dil)));
        ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
        for j = 1:length(ctmix)
            text(x(j)+0.05,ctmix(j),num2str(j));
        end
        figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'corrected.pdf'];
%             pause

% --- Copier jusque l? ---                
% --- Coller ici

    else % Copier avant ce 'else'
        try
            % Recuperer les Ct de la courbe 
            ctmix = Ct(CG_G==i & strcmp('gamme',SName));
            % Recuperer les dilutions de la courbe
            dil = rConc(CG_G==i & strcmp('gamme',SName));

            % Calculer la pente b(2) intersection b(1) 
            b = robustfit(log10(1./dil'),ctmix);
            % Calculer l'efficacite
            eff(i) = 10^(-1/b(2));
            
            % Dessiner la figure
            figure
            k = scatter(log10(1./dil'),ctmix,'filled'); grid on; hold on
            x = log10(1./dil');
            plot(x,b(1)+b(2)*x,'g','LineWidth',2)
            title ({['Gene ',num2str(i)];CG_GN{i};['Efficacity = ',num2str(eff(i),'%0.03f')]})
            ax = gca;
            ax.YLabel.String = 'C_t';
            ax.XLabel.String = 'Dilution';
            ax.XTick = sort(unique(log10(1./dil)));
            ax.XTickLabel = {'1','1/2','1/4','1/8','1/16'};
            for j = 1:length(ctmix)
                text(x(j)+0.05,ctmix(j),num2str(j));
            end
            figName=['Gene_',num2str(i,'%02.0f'),'_',CG_GN{i},'allpoints.pdf'];
%             pause

        catch
            display(['/!\ A VERIFIER GENE ',num2str(i)])
        end
    end
    % Sauver la figure
    clear dil
    saveas(gcf,fullfile(resPath,'pdf new',figName));
    close
end

%% Analyser les efficacites
% Distribution
histogram(eff,25)
ax = gca;
ax.XLabel.String  = 'Efficacity';
ax.YLabel.String = 'Occurence';
saveas (gcf,'20180417_histogram_1.png')
close
% Find outliers
bad = eff>1;
% find(bad)
% 33
histogram(eff(not(bad)),25)
ax = gca;
ax.XLabel.String  = 'Efficacity';
ax.YLabel.String = 'Occurence';
saveas (gcf,'20180417_histogram_2.png')
close
bad2 = eff<0.25;
% find(bad2)
% 91 97 148 155 186 188 190
% Sauver efficacite
EFF = [CG_GN, num2cell(eff)];
resultFile = ['EFFICACITE_',datestr(now,'yyyymmdd-HHMMSS'),'.xls'];
fileID = fopen(fullfile(resPath,resultFile),'w');
% Write headers
fprintf(fileID,'%s %s\n','Gene Name','Efficacite');
% Write data
for i= 1:numel(CG_GN)
    fprintf(fileID,'%s\t',CG_GN{i});
    fprintf(fileID,'%6.3f\n',eff(i));
end
fclose(fileID);

%% Sauvegarde intermediaire
save ('Analysis_Fluidigm_15EX05_Pl01_10_tmp.mat');
%% Observation des genes de reference
% 
% Rappel ID des genes de reference
% HanXRQChr04g0115631
% HanXRQChr03g0090171
% HanXRQChr01g0021131
% HanXRQChr05g0131911
% HanXRQChr01g0029581

% % Evaluer la variabilite des genes de references apres correction par un
% effet plaque
% Pl = rawdata.Plate;
ref =  find(contains(CG_GN,'=ref'));
for i = 1:numel(ref)
    ct = Ct(CG_G==ref(i));
    pl = Pl(CG_G==ref(i));
    g = ones(size(ct));
    tbl = table(ct,pl,g,...
        'VariableNames',{'Ct','Plate','Group'} );
    % Linear mixed model
    lm = fitlme(tbl,'Ct ~ 1 + (1|Plate)');
    RESIDUALS(:,i) = lm.residuals;
    MSE(i) = lm.MSE;
end
% MSE =
% 
%     2.7223    1.6324   13.9149    2.9755    3.1769
% Plot variability of residuals
boxplot(RESIDUALS)
ax = gca;
ax.YLabel.String = 'Residuals of C_t after plate correction';
ax.XTickLabel = CG_GN(ref);
ax.XTickLabelRotation = 30;
saveas(gcf,'20180417_Boxplot_reference_genes.png')
close
% Reference genes 3 is too variable
ref(3:4) = [];

%% Verifier que l'on a toutes les donnees pour les genes de reference
clear cgref ctref plref stref snref
for i = 1:numel(ref)
    cgref(:,i) = CG(CG_G==ref(i));
    ctref(:,i) = Ct(CG_G==ref(i));
    plref(:,i) = Pl(CG_G==ref(i));
    stref(:,i) = SType(CG_G==ref(i));
    snref(:,i) = SName(CG_G==ref(i));
end

% Enlever la gamme et l'eau et l'ADNg
bad = strcmp(snref(:,1),'H2O')|strcmp(snref(:,1),'gamme')|strcmp(snref(:,1),'ADNg');
cgref(bad,:) = [];
ctref(bad,:) = [];
plref(bad,:) = [];
stref(bad,:) = [];
snref(bad,:) = [];
% NB echantillons avec genes de ref 
% sum(sum((isnan(ctref)),2)==0)
% 3    2    1    0
% 439  6    0    0

% Calculer la moyenne du gene de ref pour les echantillons ayant toutes
% les donnees
all = find(sum(isnan(ctref),2)==0);
ctref_mean = mean(ctref(all,:));
% 17.4364   15.3633   20.2143

% Remplacer les donnes manquantes par la moyenne du gene de reference sur
% les autres echantillons
ctref_corr = ctref;
% ctref_iscorr = isnan(ctref);
for i = 1:numel(ref)
    ctref_corr(isnan(ctref(:,i)),i) = ctref_mean(i);
end

% Modifier Ct
Ct_corr = Ct;
for i  = 1:size(ctref,1)
    for j = 1:size(ctref,2)
        if isnan(ctref(i,j))
            g = strcmp(CG,cgref{1,j});
            s = strcmp(SName,snref{i,1});
            Ct_corr(g&s) = ctref_corr(i,j);
        end
    end
end

%% Save data
save ('Analysis_Fluidigm_15EX05_Pl01_10_tmp2.mat');


%% Calculer le deltaCt(rapport entre le Ct_corrig? par efficacit? et la moyenne des Ct des g?nes de ref corrig?s par chacune de leur efficacit?
%Ref={}; %copier coller la colonne avec la categorie du gene: Ref, CG ou NRC
%clear eff 
% eff_ref=eff(strncmp('CG',CG_GN(:,1),2)); %r?cup?rer les efficacit?s des g?nes de r?f?rences en comparant les noms des g?nes (seuls les g?nes de ref ont un nom commen?ant par CG
% eff_ref2=eff2(strncmp('CG',CG_GN(:,1),2));
% DCt1=[]; % Objet pour ranger les valeurs de DCt calcul?es par un rapport
% DCt2=[];
% Mean_Ctref=[];
% Mean_Ctref2=[];% Objet pour le calcul de la moyenne des Ct des g?nes de ref corrig?s par leur efficacit? respective.
DCt = nan(size(Ct));
for i = 1:numel(Ct_corr)
    % Trouver les Ct correspondant aux genes de ref pour l'echantillon
    % considere
    Ctref = [nanmean(Ct_corr(CG_G==ref(1)&SN_G==SN_G(i)&rConc==rConc(i))),...
        nanmean(Ct_corr(CG_G==ref(2)&SN_G==SN_G(i)&rConc==rConc(i))),...
        nanmean(Ct_corr(CG_G==ref(3)&SN_G==SN_G(i)&rConc==rConc(i)))];
    % Calculer dCt
    DCt(i) = ((1+eff(CG_G(i)))^-Ct(i))/...
        mean([(1+eff(ref(1)))^-Ctref(1),...
        (1+eff(ref(2)))^-Ctref(2),...
        (1+eff(ref(3)))^-Ctref(3)]);
end

%% Tag genes avec mauvaises gamme etalon
% 33 91 97 148 155 186 188 190
bad = [33 91 97 144 148 155 186 188 190];
Quality = true(size(Ct));
for i = 1:numel(bad)
    Quality(CG_G==bad(i)) = false;
end


%% Get the correspondances
load (fullfile(rootPath,'15EX05_Correspondance.mat'));
for i = 1:size(Plot,1)
    idx = find(Corres.PLOT==Plot(i,2));
    Corres.SName(idx) = Plot(i,1);
end

% Get data
clear INTERN_ID XTRIAL YTRIAL PLOT CROSS_GENOTYPE STATUS_EXP
for i = 1:numel(Ct)
    idx = find(Corres.SName==str2double(SName(i)));
    if isempty(idx)
        INTERN_ID(i,1) = categorical({'NA'});
        XTRIAL(i,1) = NaN;
        YTRIAL(i,1) = NaN;
        PLOT(i,1) = NaN;
        CROSS_GENOTYPE(i,1) = categorical({'NA'});
        STATUS_EXP(i,1) = categorical({'NA'});
    else
        INTERN_ID(i,1) = Corres.INTERN_ID(idx);
        XTRIAL(i,1) = Corres.XTRIAL(idx);
        YTRIAL(i,1) = Corres.YTRIAL(idx);
        PLOT(i,1) = Corres.PLOT(idx);
        CROSS_GENOTYPE(i,1) = Corres.CROSS_GENOTYPE(idx);
        STATUS_EXP(i,1) = Corres.STATUS_EXP(idx);
    end
end

%% SAVE DATA
save (fullfile(resPath,'Analysis_Fluidigm_15EX05_results.mat'))

%% Export data
tbl = table(SName,Pl,SType,CG,Ct,Ct_corr,DCt,Quality,...
    INTERN_ID, XTRIAL, YTRIAL, PLOT, CROSS_GENOTYPE, STATUS_EXP,...
    'VariableNames',{'Sample_Name','Plate','Sample_Type','Gene','Ct','Ct_corrected','dCt','Gene_Quality',...
    'INTERN_ID', 'XTRIAL', 'YTRIAL', 'PLOT', 'CROSS_GENOTYPE', 'STATUS_EXP'});
writetable(tbl,...
    fullfile(resPath,['Analysis_Fluidigm_15EX05_results_export_',datestr(now,'yyyymmdd-HHMMSS'),'.txt']),...
    'Delimiter','\t');
