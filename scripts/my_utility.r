
my_create_res_exp_dir <- function(outDir,exp){
  # Create a subdirectory named after a given argument in a given result directory.
  #
  # Args:
  #   outdir : character, the path to the result directory in which to create the experiment directory
  #   exp : the name of the experiment
  #
  # Return:
  #    newDir
  newDir <- file.path(outDir, exp)
  
  if(!file.exists(newDir)){dir.create(newDir)}
  
  return(newDir)
}

#------------------------------------------------------------------------#

my_load_expe_file <- function(dir,exp_file, NAs = c("x","#VALUE!","NA",""), sep = ","){
  # Open a file conatining for muli-genotypes different information
  # collect from an experiment
  #
  # Args:
  #   dir: directory where the file is store
  #   file: the name of the experiment file
  #   NAs: character vector, stored the values that should be interpreted as NA during the file loading default is c("x","#VALUE!","NA","")
  #   sep: character separating col, by default ","
  #
  # Return:
  #   data : dat of the experiment
  
  data <- read.csv(file=file.path(dir,exp_file), sep = sep, stringsAsFactors = FALSE, na.strings = NAs) 
  return(data)
}

#------------------------------------------------------------------------#


my_reformat_expe_data <- function(data, checks = c("ES_AKUSTIC","EXTRASOL","LG5450HO","NK_KONDI"), makerows = TRUE, createRILcol = TRUE){
  # Reformat the experimental data,
  # (1) check and remove wrong sample,
  # (2) create new sample names based on sample type (exp or check) and position in field
  # (3) add column to data
  #
  # Args:
  #   data: data frame contains experiment informations, mandatory columns are:
  #         -   STATUS_EXP with either exp or check as values (regardless of case). NA and unexpected values will be removed from the data frame
  #         -   XTRIAL and YTRIAL the values should be numerical values. No two plots can have both the same value in XTRIAL and YTRIAL. In other words, two plots cannot have the same coordinates
  #         -   CROSS_GENOTYPE, hybrid should follow this formatting RHA274_SF109xSF317.24 (TESTER_RILPOP.RILNUMBER). Checks should be either "ES_AKUSTIC","EXTRASOL","LG5450HO" or "NK_KONDI"
  #   makerows: two possible values:
  #         -   TRUE: new sample add ar used as line name in data
  #         -   FALSE: new sample name are placed in a new colomn
  # Returns:
  #    the data frame with a concatenation of STATUS_EXP, XTRIAL and YTRIAL as rownames and four new columns, "TESTER_GENOTYPE","RIL_POP","RIL_NUMBER" and "CONTROL"
  
  data$STATUS_EXP <- toupper(data$STATUS_EXP) # uppercase status exp
  
  ### 1 - Remove STATUS_EXP unexpected values (not "exp" or "check")
  
  # check if all status_exp value are either EXP or CHECK
  if(any(!is.element(data$STATUS_EXP,c("EXP","CHECK")))){
    print(paste0("WARNING: ", exp, " STATUS_EXP with unexpected value")) # printout warning message if a unexpected value in found
    print(paste0("Data from these plots will be removed:")) 
    print(data[!is.element(data$STATUS_EXP,c("EXP","CHECK")),"PLOT"]) # printout plots that do not follow the expected STATUS_EXP formatting
  }
  
  # remove plots that do not follow the expected STATUS_EXP formatting
  data <- data[is.element(data$STATUS_EXP,c("EXP","CHECK")),]
  
  ### 2 - create unique row names
  
  #The row name is a concatenation of STATUS_EXP, field column name and field line name (XTRIAL x YTRIAL Combination should be unique)
  newrows <- apply(data,1,function(r){
    temp <- paste(r[c("STATUS_EXP","XTRIAL","YTRIAL")], collapse = "_", sep = "") 
    temp <- gsub(" ", "", temp)       # removing spaces from the rownames
  })
  
  # test value of option makerows
  if(makerows){
    # TRUE : rename lignes
    rownames(data) <- newrows #ADD NEWROWS TO DATA FRAME IF THIS STEP FAILS THEN THERE ARE DISREPANCIES IN THE PLOT FILE
  }
  else{
    # FALSE : add a new column
    data$MY_PLOT_CODE <- newrows
  }
  
  ### 3 - Creation of new colomn
  if(createRILcol){
    # Create the TESTER_GENOTYPE, RIL_POP, RIL_NUMBER and CONTROL columns
    ril_tester_col <- lapply(data$CROSS_GENOTYPE, function(g){
      res <- unlist(strsplit(as.character(g), "[._]")) # create vector with the 2 parents name
      
      # Dealing with chek genotypes
      if (length(res) == 1){ # if the genotype is a check genotype "EXTRASOL" or "LG5450HO"
        res <- c(NA, res, NA, res)
      }else{
        if (length(res) == 2){ # if the genotype is a check genotype "ES_AKUSTIC" or "NK_KONDI"
          res <- c(NA, paste(res, collapse = "_"), NA, paste(res, collapse = "_"))
        }else{ # if the genotype is an hybrid genotype
          res <- c(res, "RIL")
        }}
      res
    })
    
    #Create the data frame with ...
    ril_tester_col <- data.frame(matrix(unlist(ril_tester_col), nrow <- dim(data)[1], byrow=T))
    # ... c("TESTER_GENOTYPE","RIL_POP","RIL_NUMBER","CONTROL") as columns
    colnames(ril_tester_col) <- c("TESTER_GENOTYPE","RIL_POP","RIL_NUMBER","CONTROL")
    
    # complete data with the new column
    data <- cbind(data,ril_tester_col) #cbind the final data frame
  }
  
  return(data)
}

#------------------------------------------------------------------------#


cleaning_plots = function(tab, threshold = 3){
  # Cleaning the experimental data to remove sample where the plant density,
  # or the quality of sample is too low. Two mandatory colomns PLANT_DENSITY & QUALITY,
  # if PLANT_DENSITY is not in the data frame column,returns the same data frame
  #
  # Args:
  #   tab: data frame with experimental data, request colomns PLANT_DENSITY & QUALITY 
  #   threshold: PLANT_DENSITY threshold, if the PLANT_DENSITY of a plot is below this threshold it will be removed
  #
  # Returns:
  #   tab: data frame without the filtered plots
  
  
  if("PLANT_DENSITY" %in% names(tab)){
    print("Cleaning on PLANT_DENSITY")
    tab[is.na(tab$PLANT_DENSITY),"PLANT_DENSITY"] <- 0
    filter <- tab$PLANT_DENSITY >= threshold
    filtered <- tab[!filter,]
    print(paste("PLANT_DENSITY filter, number of EXP plots removed:", sum(ifelse(filtered$STATUS_EXP == "EXP",1,0))))
    print(paste("PLANT_DENSITY filter, number of CHECK plots removed:", sum(ifelse(filtered$STATUS_EXP == "CHECK",1,0))))
    tab <- tab[filter,]
  }
  
  if("QUALITY" %in% names(tab)){
    print("Cleaning on QUALITY")
    tab[is.na(tab$QUALITY),"QUALITY"] <- 0
    filter <- tab$QUALITY > 0
    filtered <- tab[!filter,]
    print(paste("QUALITY filter, number of EXP plots removed:", sum(ifelse(filtered$STATUS_EXP == "EXP",1,0))))
    print(paste("QUALITY filter, number of CHECK plots removed:", sum(ifelse(filtered$STATUS_EXP == "CHECK",1,0))))
    tab <- tab[filter,]
  }
  
  tab
}

#------------------------------------------------------------------------#

my_add_empty_plots <- function(data){
  # Add empty sample (with NA) to build a rectangular field for autoregressive models with ASREML
  #
  # Args:
  #   data: data frame, must be a my_reformat_expe_data output
  #
  # Return:
  #   data: data completed with NA sample too have a complete field
  
  data$YTRIAL <- as.numeric(as.character(data$YTRIAL))
  data$XTRIAL <- as.numeric(as.character(data$XTRIAL))
  
  for(col in min(data$XTRIAL, na.rm = T):max(data$XTRIAL, na.rm = T)){
    for(row in min(data$YTRIAL, na.rm = T):max(data$YTRIAL, na.rm = T)){
      if(!any(data$XTRIAL == col & data$YTRIAL == row)){
        newrow <- rep(NA,dim(data)[2])
        names(newrow) <- colnames(data)
        newrow["XTRIAL"] <- col
        newrow["YTRIAL"] <- row
        newrowname <- paste("NA",col,row, sep = "_")
        data[newrowname,] <- newrow
      }
    }
  }
  return(data)
}

#------------------------------------------------------------------------#

my_prepare_data_for_model <- function(data){
  # Add XTRIAL_int and YTRIAL_int, the equivalent of XTRIAL and YTRIAL as a numerical value for runnign SpATS
  # Changes, YTRIAL, XTRIAL, CROSS_GENOTYPE, CONTROL to factors
  #
  # Args:
  #   data: data frame, must be a my_add_empty_plots output
  #
  # Returns:
  #   data.for.model: data prep to be used for mdelisation
  
  data.for.model <- data
  data.for.model$CROSS_GENOTYPE <- as.factor(data.for.model$CROSS_GENOTYPE)
  
  data.for.model$YTRIAL_int <- as.integer(data.for.model$YTRIAL)
  data.for.model$XTRIAL_int <- as.integer(data.for.model$XTRIAL)
  
  data.for.model$YTRIAL <- as.factor(data.for.model$YTRIAL)
  data.for.model$XTRIAL <- as.factor(data.for.model$XTRIAL)
  
  data.for.model <- data.for.model[order(data.for.model$YTRIAL, data.for.model$XTRIAL),]
  
  return(data.for.model)
}

#------------------------------------------------------------------------#

my_ASREML_add_effects <- function(data, model_obj, fit_name, checks, mean_trait = 0, effname = "CROSS_GENOTYPE",random = FALSE){
  # This function uses predict from ASREML to create the result table,
  # it uses the same argument from the previous function. Please comment one or the other.
  #
  # Args:
  #   data:
  #   model_obj:
  #   fit_name:
  #   checks:
  #   mean_trait: (default 0)
  #   effname: (default "CROSS_GENOTYPE")
  #   random: (default FALSE)
  #
  # Return:
  #
  
  pred <- predict(model_obj, classify = effname, pworkspace = 64e6)
  pred <- pred$predictions$pvals[c(effname,"predicted.value")]
  
  colnames(pred) <- c(effname,fit_name)
  result_df <- merge(data, pred, by.x = c(effname), by.y = c(effname), sort = FALSE, all = TRUE)
  result_df
}

#------------------------------------------------------------------------#

# /!\ Not used, problems with some packages
summary_fitted_values <- function(data.for.model,m1,exp,name.trait, m_name = "m1",check_genotypes = c("ES_AKUSTIC","EXTRASOL","LG5450HO","NK_KONDI")){
  # Desc
  # Need personal function to work:
  #  - my_ASREML_add_effects()
  #  - plot_end_summary()
  #  - 
  #
  # Args:
  #   data.for.model: data frame, must be a my_prepare_data_for_model output
  #   m1: Asreml result for the studied gene/phenotype
  #   exp: name of the experiment
  #   name.trait: name of the gene/trait/phenotype studied
  #   m_name: name of the model (default "m1")
  #   check_genotypes: vector of check genotype (default c("ES_AKUSTIC","EXTRASOL","LG5450HO","NK_KONDI"))
  #
  # Return:
  #
  
  plot_df <- data.for.model[c("CROSS_GENOTYPE","PHENOTYPE","XTRIAL","YTRIAL")]
  
  plot_df$m1_res <- m1$residuals
  plot_df <- my_ASREML_add_effects(plot_df, m1, "m1_fit")
  
  colnames(data.for.model) <- gsub(colnames(data.for.model), pattern = "PHENOTYPE" ,replacement =  name.trait)
  colnames(plot_df) <- gsub(colnames(plot_df), pattern = "PHENOTYPE" ,replacement =  name.trait)
  
  filename <- file.path(plotDir, paste(exp,name.trait,"ADJ_SUMMARY.png"))
  png(filename, width = 10, height = 10, units = 'in',res = 300)
  plot_end_summary(data.for.model, plot_df, name.trait, exp)
  dev.off()
  
  corr_data <- plot_df[c("CROSS_GENOTYPE",name.trait,"m1_fit")]
  corr_data <- corr_data[!is.element(corr_data$CROSS_GENOTYPE, check_genotypes), c(name.trait,"m1_fit")]
  
  # /!\ PROBLEM with GGaly, not installed on LIPM-calcul /!\ ############
  # filename <- file.path(plotDir,paste(exp,name.trait,"pairs.png",sep = "_"))
  # png(filename, width = 10, height = 10, units = 'in',res = 300)
  # print(ggpairs(corr_data))
  # dev.off()
  #######################################################################
  
  summary_df <- data.frame(AIC = double(), z.ratio_XTRIAL = double(), z.ratio_YTRIAL = double(), "var_fitted.var_raw" = double()) 
  summary_df[m_name,] <- NA
  # /!\ asremlPlus is missing ###########################################
  ### summary_df[m_name,"AIC"] <- unlist(info.crit.asreml(m1)["AIC"])
  #######################################################################
  summary_df[m_name,"z.ratio_XTRIAL"] <- summary(m1)$varcomp["XTRIAL!XTRIAL.var","z.ratio"]
  summary_df[m_name,"z.ratio_YTRIAL"] <- summary(m1)$varcomp["YTRIAL!YTRIAL.var","z.ratio"]
  summary_df[m_name,"var_fitted.var_raw"] <- var(corr_data$m1_fit, na.rm = TRUE)/var(corr_data[,name.trait], na.rm = TRUE)
  
  filename <- file.path(plotDir,paste(exp,name.trait,"summary_table.csv",sep = "_"))
  write.csv(summary_df,file = filename)
}

#------------------------------------------------------------------------#

my_ASREML_add_effects <- function(data, model_obj, fit_name, checks, mean_trait = 0, effname = "CROSS_GENOTYPE",random = FALSE){
  # This function uses predict from ASREML to create the result table
  #
  # Args:
  #   data:
  #   model_obj:
  #   fit_name:
  #   checks:
  #   mean_trait: (default 0)
  #   effname: (default "CROSS_GENOTYPE")
  #   random: (default FALSE)
  #
  # Return:
  #   
  
  pred <- predict(model_obj, classify = effname, pworkspace= 64e6)
  pred <- pred$predictions$pvals[c(effname,"predicted.value")]
  colnames(pred) <- c(effname,fit_name)
  result_df <- merge(data, pred, by.x = c(effname), by.y = c(effname), sort = FALSE, all = TRUE)
}

#------------------------------------------------------------------------#


plot_end_summary <- function(rawdata, adj_data, name.trait, exp){
  #
  #
  # Args:
  #   rawdata:
  #   adj_data:
  #   name.trait:
  #   sort: (default FALSE)
  #   all: (default TRUE)
  #
  # Return:
  #
  
  all_values <- as.numeric(unlist(c(rawdata[,name.trait],adj_data[,"m1_fit"])))
  min_val <- min(all_values,  na.rm = TRUE)
  max_val <- max(all_values,  na.rm = TRUE)
  mid_val <- mean(all_values,  na.rm = TRUE)
  
  rawplot <- my_plot_heatmap(rawdata, name.trait, title = "RAW VALUES", titlesize = 15, min_val = min_val, max_val = max_val, mid_val = mid_val)
  fitmap1 <- my_plot_heatmap(adj_data,"m1_fit", title = "FITTED VALUES", titlesize = 15, min_val = min_val, max_val = max_val, mid_val = mid_val)
  resmap <- my_plot_heatmap(adj_data,"m1_res", title = "RESIDUALS", titlesize = 15)
  t <- textGrob(paste(exp,name.trait))
  
  plot(grid.arrange(t, rawplot, fitmap1, resmap,  ncol=2, nrow = 2))
}

#------------------------------------------------------------------------#
#my_plot_heatmap
my_plot_heatmap <- function(data, trait, title = "", subtitle = "", fillab = "", lowColor = "blue", midcolor = "green", highColor = "yellow", titlesize = 8, nacolor = "white", min_val = NA, max_val = NA, mid_val = NA){
  # Plots a field heatmap
  #
  # Args:
  #   data:  data frame containing plotting info
  #   trait: the name of the column from data containing the vector to plot (usualy raw trait or residuals from adjustment models)
  #   title: Plot title
  #   subtitle: plot subtitle
  #   fillab: Changes the name of the plot legend
  #   lowcolor, midcolor, highcolor: set the color range for the heatmap
  #   titlesize: set the title size
  #   nacolor: set the color for NA values
  #   min_val = NA, max_val = NA, mid_val = NA
  
  data$XTRIAL <- as.numeric(data$XTRIAL)
  data$YTRIAL <- as.numeric(data$YTRIAL) 
  
  if(is.na(min_val)){
    min_val <- min(data[,trait], na.rm = TRUE)
  }
  
  if(is.na(max_val)){
    max_val <- max(data[,trait], na.rm = TRUE)
  }
  
  if(is.na(mid_val)){
    mid_val <- mean(data[,trait], na.rm = TRUE)
  }
  
  ggplot(data = data, aes(x = XTRIAL, y = YTRIAL, fill = data[,trait])) +
    geom_tile() +
    scale_fill_gradient2(low = lowColor, mid = midcolor, high = highColor, 
                         midpoint = mid_val,
                         limit = c(min_val,max_val),
                         na.value = nacolor
    ) +
    labs(fill = fillab,
         title = title,
         subtitle = subtitle) +
    theme(plot.title = element_text(size=titlesize))
  
}
